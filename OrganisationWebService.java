package studentorganisations;

import ee.ttu.idu0075._2015.ws.organisation.AddOrganisationRequest;
import ee.ttu.idu0075._2015.ws.organisation.AddOrganisationStudentRequest;
import ee.ttu.idu0075._2015.ws.organisation.AddStudentRequest;
import ee.ttu.idu0075._2015.ws.organisation.GetOrganisationListRequest;
import ee.ttu.idu0075._2015.ws.organisation.GetOrganisationListResponse;
import ee.ttu.idu0075._2015.ws.organisation.GetOrganisationRequest;
import ee.ttu.idu0075._2015.ws.organisation.GetOrganisationStudentListRequest;
import ee.ttu.idu0075._2015.ws.organisation.GetStudentListRequest;
import ee.ttu.idu0075._2015.ws.organisation.GetStudentListResponse;
import ee.ttu.idu0075._2015.ws.organisation.GetStudentRequest;
import ee.ttu.idu0075._2015.ws.organisation.OrganisationStudentListType;
import ee.ttu.idu0075._2015.ws.organisation.OrganisationStudentType;
import ee.ttu.idu0075._2015.ws.organisation.OrganisationType;
import ee.ttu.idu0075._2015.ws.organisation.StudentType;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.jws.WebService;

/**
 *
 * @author Mari-Liis
 */
@WebService(serviceName = "StudentOrganisationService", portName = "StudentOrganisationPort", endpointInterface = "ee.ttu.idu0075._2015.ws.organisation.StudentOrganisationPortType", targetNamespace = "http://www.ttu.ee/idu0075/2015/ws/organisation", wsdlLocation = "WEB-INF/wsdl/OrganisationWebService/StudentOrganisationService.wsdl")
public class OrganisationWebService {
    
    static int nextStudentId = 1;
    static int nextOrganisationId = 1;
    static List<StudentType> studentList = new ArrayList<StudentType>();
    static List<OrganisationType> organisationList = new ArrayList<OrganisationType>();
    static HashMap<BigInteger, StudentType> studentRequestMap = new HashMap<BigInteger, StudentType>();
    static HashMap<BigInteger, OrganisationType> organisationRequestMap = new HashMap<BigInteger, OrganisationType>();
    static HashMap<BigInteger, OrganisationStudentType> organisationStudentRequestMap = new HashMap<BigInteger, OrganisationStudentType>();

    public StudentType getStudent(GetStudentRequest parameter) {
        StudentType pt = null;
        if (parameter.getToken().equalsIgnoreCase("TTÜ")) {
            for (int i = 0; i < studentList.size(); i++) {
                if (studentList.get(i).getId().equals(parameter.getId())) {
                    pt = studentList.get(i);
                }
            }    
        }
        return pt;
    }

    public StudentType addStudent(AddStudentRequest parameter) {
        StudentType pt = new StudentType();
        if (parameter.getToken().equalsIgnoreCase("TTÜ")) {
            if (studentRequestMap.containsKey(parameter.getRequestId())) {
                pt = studentRequestMap.get(parameter.getRequestId());
            } else {
            pt.setName(parameter.getName());
            pt.setCode(parameter.getCode());
            pt.setAge(parameter.getAge());
            pt.setPhone(parameter.getPhone());
            pt.setEmail(parameter.getEmail());
            pt.setId(BigInteger.valueOf(nextStudentId++));
            studentList.add(pt);
            studentRequestMap.put(parameter.getRequestId(), pt);
        }
        }
        return pt;
    }

    public GetStudentListResponse getStudentList(GetStudentListRequest parameter) {
        GetStudentListResponse response = new GetStudentListResponse();
        if (parameter.getToken().equalsIgnoreCase("TTÜ")) {
            for (StudentType studentType : studentList) {
                response.getStudent().add(studentType);
            }
        }
        return response;
    }

    public OrganisationType getOrganisation(GetOrganisationRequest parameter) {
        OrganisationType it = null;
        if (parameter.getToken().equalsIgnoreCase("TTÜ")) {
            for (int i = 0; i < organisationList.size(); i++) {
                if (organisationList.get(i).getId().equals(parameter.getId())) {
                    it = organisationList.get(i);
                }

            }    
        }
        return it;
    }

    public OrganisationType addOrganisation(AddOrganisationRequest parameter) {
        OrganisationType it = new OrganisationType();
        if (parameter.getToken().equalsIgnoreCase("TTÜ")) {
            if (organisationRequestMap.containsKey(parameter.getRequestId())) {
                it = organisationRequestMap.get(parameter.getRequestId());
            } else {
            it.setId(BigInteger.valueOf(nextStudentId++));
            it.setName(parameter.getName());
            it.setAddress(parameter.getAddress());
            it.setEmail(parameter.getEmail());
            it.setPhone(parameter.getPhone());
            it.setField(parameter.getField());
            it.setOrganisationStudentList(new OrganisationStudentListType());
            organisationList.add(it);
            organisationRequestMap.put(parameter.getRequestId(), it);
        }
        }
        return it;
    }

    public GetOrganisationListResponse getOrganisationList(GetOrganisationListRequest parameter) {
        GetOrganisationListResponse response = new GetOrganisationListResponse();
        if (parameter.getToken().equalsIgnoreCase("TTÜ")) {
            for (OrganisationType organisationType :organisationList) {
                if (((parameter.getHasRelatedStudents().equalsIgnoreCase("ei") && (organisationType.getOrganisationStudentList() == null || organisationType.getOrganisationStudentList().getOrganisationStudent().isEmpty()))
                        || (parameter.getHasRelatedStudents().equalsIgnoreCase("jah") && organisationType.getOrganisationStudentList() != null && !organisationType.getOrganisationStudentList().getOrganisationStudent().isEmpty())) 
                    && (parameter.getStatus().equalsIgnoreCase("aktiivne"))
                        ){
                    response.getOrganisation().add(organisationType);
                }
            }
        }
        return response;
    }

    public OrganisationStudentListType getOrganisationStudentList(GetOrganisationStudentListRequest parameter) {
        OrganisationStudentListType OrganisationStudentList = null;
        if (parameter.getToken().equalsIgnoreCase("TTÜ")) {
            for (int i = 0; i < organisationList.size(); i++) {
                if (organisationList.get(i).getId().equals(parameter.getOrganisationId())) {
                    OrganisationStudentList = organisationList.get(i).getOrganisationStudentList();
                }
            }    
        }
        return OrganisationStudentList;
    }

    public OrganisationStudentType addOrganisationStudent(AddOrganisationStudentRequest parameter) {
        OrganisationStudentType organisationStudent = new OrganisationStudentType();
        if (parameter.getToken().equalsIgnoreCase("TTÜ")) {
            if (organisationStudentRequestMap.containsKey(parameter.getRequestId())) {
                organisationStudent = organisationStudentRequestMap.get(parameter.getRequestId());
            } else {
            GetStudentRequest studentRequest = new GetStudentRequest();
            studentRequest.setId(parameter.getStudentId());
            studentRequest.setToken(parameter.getToken());
            organisationStudent.setStudent(getStudent(studentRequest));
            organisationStudent.setJoiningYear(parameter.getJoiningYear());
            
          
  
        
            for (int i = 0; i < organisationList.size(); i++) {
                if (organisationList.get(i).getId().equals(parameter.getOrganisationId())) {
                    organisationList.get(i).getOrganisationStudentList().getOrganisationStudent().add(organisationStudent);
                    
                    return organisationStudent;
                }
                organisationStudentRequestMap.put(parameter.getRequestId(), organisationStudent);

            } 
        }
        }
        return null;
    }
    
}