package studentorganisations;

import ee.ttu.idu0075._2015.ws.organisation.AddStudentRequest;
import ee.ttu.idu0075._2015.ws.organisation.GetStudentListRequest;
import ee.ttu.idu0075._2015.ws.organisation.GetStudentListResponse;
import ee.ttu.idu0075._2015.ws.organisation.GetStudentRequest;
import ee.ttu.idu0075._2015.ws.organisation.StudentType;
import java.math.BigInteger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;

/**
 * REST Web Service
 *
 * @author Mari-Liis
 */
@Path("students")
public class StudentsResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of StudentsResource
     */
    public StudentsResource() {
    }

    @GET
    @Produces("application/json")
    public GetStudentListResponse getStudentList(@QueryParam("token") String token) {
        OrganisationWebService ws = new OrganisationWebService();
        GetStudentListRequest request = new GetStudentListRequest();
        request.setToken(token);
        return ws.getStudentList(request);
    }
    
    @GET
    @Path("{id : \\d+}") //supports digits only
    @Produces("application/json")
    public StudentType getStudent(@PathParam("id") String id,
            @QueryParam("token") String token) {
        OrganisationWebService ws = new OrganisationWebService();
        GetStudentRequest request = new GetStudentRequest();
        request.setId(BigInteger.valueOf(Integer.parseInt(id)));
        request.setToken(token);
        return ws.getStudent(request);
    }
    
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public StudentType addStudent(StudentType content, 
                                @QueryParam("token") String token,
                                @QueryParam("requestId") BigInteger requestId) {
        OrganisationWebService ws = new OrganisationWebService();
        AddStudentRequest request = new AddStudentRequest();
        request.setCode(content.getCode());
        request.setName(content.getName());
        request.setAge(content.getAge());
        request.setEmail(content.getEmail());
        request.setPhone(content.getPhone());
        request.setToken(token);
        request.setRequestId(requestId);
        return ws.addStudent(request);
    }
}