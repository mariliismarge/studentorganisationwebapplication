package studentorganisations;


import ee.ttu.idu0075._2015.ws.organisation.AddOrganisationRequest;
import ee.ttu.idu0075._2015.ws.organisation.AddOrganisationStudentRequest;
import ee.ttu.idu0075._2015.ws.organisation.GetOrganisationListRequest;
import ee.ttu.idu0075._2015.ws.organisation.GetOrganisationListResponse;
import ee.ttu.idu0075._2015.ws.organisation.GetOrganisationRequest;
import ee.ttu.idu0075._2015.ws.organisation.GetOrganisationStudentListRequest;
import ee.ttu.idu0075._2015.ws.organisation.OrganisationStudentListType;
import ee.ttu.idu0075._2015.ws.organisation.OrganisationStudentType;
import ee.ttu.idu0075._2015.ws.organisation.OrganisationType;
import java.math.BigInteger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.POST;
import javax.ws.rs.QueryParam;

/**
 * REST Web Service
 *
 */
@Path("organisations")
@RequestScoped
public class OrganisationsResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of InvoicesResource
     */
    public OrganisationsResource() {
    }

    /**
     * Retrieves representation of an instance of invoice.InvoicesResource
     * @param token
     * @param hasRelatedStudents
     * @param status
     
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/json")
    public GetOrganisationListResponse getOrganisationList(@QueryParam("token") String token, 
            @QueryParam("hasRelatedStudents") String hasRelatedStudents,
            @QueryParam("status") String status){
        OrganisationWebService ws = new OrganisationWebService();
        GetOrganisationListRequest request = new GetOrganisationListRequest();
        request.setToken(token);
        request.setHasRelatedStudents(hasRelatedStudents);
        request.setStatus(status);
        return ws.getOrganisationList(request);
    }

    @GET
    @Path("{id : \\d+}") //support digit only
    @Produces("application/json")
    public OrganisationType getOrganisation(@PathParam("id") String id,
            @QueryParam("token") String token) {
        OrganisationWebService ws = new OrganisationWebService();
        GetOrganisationRequest request = new GetOrganisationRequest();
        request.setId(BigInteger.valueOf(Integer.parseInt(id)));
        request.setToken(token);
        return ws.getOrganisation(request);
    }
    
    /**
     *
     * @param content
     * @param id
     * @param token
     
     * @return 
     */
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public OrganisationType addOrganisation(OrganisationType content, 
                                @QueryParam("token") String token,
                                @QueryParam("requestId") BigInteger requestId) {
        OrganisationWebService ws = new OrganisationWebService();
        AddOrganisationRequest request = new AddOrganisationRequest();
        request.setToken(token);
        request.setRequestId(requestId);
        request.setName(content.getName());
        request.setAddress(content.getAddress());
        request.setEmail(content.getEmail());
        request.setPhone(content.getPhone());
	request.setField(content.getField());
        return ws.addOrganisation(request);    
    }
    
    @GET
    @Path("{id : \\d+}/students") //support digit only
    @Produces("application/json")
    public OrganisationStudentListType getOrganisationStudentList(@PathParam("id") String id,
            @QueryParam("token") String token) {
        OrganisationWebService ws = new OrganisationWebService();
        GetOrganisationStudentListRequest request = new GetOrganisationStudentListRequest();
        request.setOrganisationId(BigInteger.valueOf(Integer.parseInt(id)));
        request.setToken(token);
        return ws.getOrganisationStudentList(request);
    }
    
    /**
     *
     * @param content
     * @param token
     * @param id
     * @return 
     */
    @POST
    @Path("{id : \\d+}/students") //support digit only
    @Consumes("application/json")
    @Produces("application/json")
    public OrganisationStudentType addOrganisationStudent(AddOrganisationStudentRequest content, 
                                @QueryParam("token") String token,
                                @PathParam("id") String id,
                                @QueryParam("requestId") BigInteger requestId) {
        OrganisationWebService ws = new OrganisationWebService();
        AddOrganisationStudentRequest request = new AddOrganisationStudentRequest();
        request.setToken(token);
        request.setRequestId(requestId);
        request.setOrganisationId(BigInteger.valueOf(Integer.parseInt(id)));
        request.setStudentId(content.getStudentId());
        request.setJoiningYear(content.getJoiningYear());
        return ws.addOrganisationStudent(request);    
    }
}